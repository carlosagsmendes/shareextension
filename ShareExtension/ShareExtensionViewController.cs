﻿using System;
using System.Drawing;

using UIKit;
using Social;
using Foundation;
using CoreFoundation;

namespace ShareExtension
{
	public partial class ShareExtensionViewController : SLComposeServiceViewController
	{
		public string SharedURL {
			get;
			set;
		}

		public string TheURL {
			get;
			set;
		}


		public ShareExtensionViewController (IntPtr handle) : base (handle)
		{
		}

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();

			// Release any cached data, images, etc that aren't in use.
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			// Do any additional setup after loading the view.
		}

		public override bool IsContentValid ()
		{
			// Do validation of contentText and/or NSExtensionContext attachments here
			return true;
		}

		public override void DidSelectPost ()
		{
			var uri = new Uri (String.Format("https://test.innovationcast.net/radar/signals/createWidget?referral={0}&title=Teste&v=2", TheURL));
			var nsurl = new NSUrl (uri.GetComponents (UriComponents.HttpRequestUrl, UriFormat.UriEscaped));
			ExtensionContext.OpenUrl (nsurl, action => {
				ExtensionContext.CompleteRequest (null, null);
			});
			UIApplication.SharedApplication.OpenUrl (nsurl);

		}

		public override SLComposeSheetConfigurationItem[] GetConfigurationItems ()
		{
			// To add configuration options via table cells at the bottom of the sheet, return an array of SLComposeSheetConfigurationItem here.
			return new SLComposeSheetConfigurationItem[0];
		}

		public string GetUrl ()
		{
			var tmpUrl = "Not Found";

			try {
				var item = ExtensionContext.InputItems [0];

				if (item != null) {
					tmpUrl = "Item Not NULL";
					foreach (NSItemProvider attachment in item.Attachments) {
						if (attachment != null) {
							if (attachment.HasItemConformingTo (MobileCoreServices.UTType.URL)) {
								attachment.LoadItem (MobileCoreServices.UTType.URL, null, (obj, err) => {
									Console.WriteLine("GOT URL");
									//ShowAlert ("Got URL", false);
									NSUrl newUrl = (NSUrl)obj;
									tmpUrl = newUrl.ToString();

								});
							} else if (attachment.HasItemConformingTo (MobileCoreServices.UTType.Image)) {
								attachment.LoadItem (MobileCoreServices.UTType.Image, null, (obj, err) => {
									tmpUrl = "IMAGE";

								});
							} else if (attachment.HasItemConformingTo(MobileCoreServices.UTType.QuickTimeMovie)) {
								attachment.LoadItem (MobileCoreServices.UTType.QuickTimeMovie, null, (obj, err) => {
									tmpUrl = "VIDEO";
								});
							}
						}
					}
				}
			} catch (Exception e) {
				ShowAlert (e.Message, false);
			}

			return tmpUrl;

		}


		public void OnItemLoaded (NSObject arg1, NSError arg2)
		{
			
		}

		public void ShowAlert(string message, bool completeRequest = true)
		{
			InvokeOnMainThread (() => {
				UIAlertController alert = UIAlertController.Create ("Share Extension", message, UIAlertControllerStyle.Alert);
				alert.AddAction (UIAlertAction.Create ("OK", UIAlertActionStyle.Default, (sender) => {
					if(completeRequest) {
						ExtensionContext.CompleteRequest (ExtensionContext.InputItems, null);
					}
				}));
				PresentViewController (alert, true, null);
			});
		}


		public override void PresentationAnimationDidFinish ()
		{
			try {
				var item = ExtensionContext.InputItems [0];
				if (item != null) {
					foreach (NSItemProvider attachment in item.Attachments) {
						if (attachment != null) {
							if (attachment.HasItemConformingTo (MobileCoreServices.UTType.URL)) {
								attachment.LoadItem (MobileCoreServices.UTType.URL, null, (obj, err) => {
									NSUrl newUrl = (NSUrl)obj;
									TheURL = newUrl.ToString();
									 
								});
							} else if (attachment.HasItemConformingTo (MobileCoreServices.UTType.Image)) {
								attachment.LoadItem (MobileCoreServices.UTType.Image, null, (obj, err) => {
									TheURL = "IMAGE";
								});
							} else if (attachment.HasItemConformingTo(MobileCoreServices.UTType.QuickTimeMovie)) {
								attachment.LoadItem (MobileCoreServices.UTType.QuickTimeMovie, null, (obj, err) => {
									TheURL = "MOVIE";
								});
							}
						}
					}
				}
			} catch (Exception e) {
				ShowAlert (e.Message, false);
			}

		}



	}
}

